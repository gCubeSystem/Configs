# Maven settings

## gcube-settings.xml
* Used by: gCube Developer
* Installed on: development machine

## jenkins-job-settings.xml
* Used by: Jenkins snapshot jobs
* When: To build a snapshot job on jenkins
* Installed on: slave nodes labeled with _CI_ and _CD_ 
* Need to be renamed to settings.xml on the node

## jenkins-staging-settings.xml
* Used by: Jenkins pipeline jobs
* When: To build a complete release with deployment on the gcube-staging-jenkins repository
* Installed on: slave nodes labeled with _CD_

## jenkins-release-settings.xml
* Used by: Jenkins pipeline jobs
* When: To build a complete release with deployment on the gcube-releases repository
* Installed on: slave nodes labeled with _CD_
