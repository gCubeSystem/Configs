# Maven settings

## gcube-developer-settings.xml
* Used by: gCube Developer
* Installed on: development machine

## jenkins-job-settings.xml
* Used by: Jenkins jobs
* Installed on: slave nodes labeled with _CI_ 
* Need to be renamed to settings.xml on the node

## jenkins-snapshots-settings.xml
* Used by: Jenkins pipeline jobs
* When: To build a complete snapshot
* Installed on: slave nodes labeled with _CD_ 

## jenkins-snapshots-dry-run-settings.xml
* Used by: Jenkins pipeline jobs
* When: To test a complete snapshot without deploying on a remote repo
* Installed on: slave nodes labeled with _CD_

## jenkins-staging-settings.xml
* Used by: Jenkins pipeline jobs
* When: To build a complete release with deployment on the gcube-staging-jenkins repository
* Installed on: slave nodes labeled with _CD_

## jenkins-staging-dry-run-settings.xml
* Used by: Jenkins pipeline jobs
* When: To test a complete release without deploying on a remote repo
* Installed on: slave nodes labeled with _CD_

## jenkins-release-dry-run-settings.xml
* Used by: Jenkins pipeline jobs
* When: To test a complete release without deploying on a remote repo
* Installed on: slave nodes labeled with _CD_

## jenkins-release-settings.xml
* Used by: Jenkins pipeline jobs
* When: To build a complete release with deployment on the gcube-releases repository
* Installed on: slave nodes labeled with _CD_
