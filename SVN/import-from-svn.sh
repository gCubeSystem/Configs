#!/bin/bash
USERNAME=
AUTHORS_FILE=authors.txt
SVN_URL=""
SVN_PROJECT_LOCAL_DIR=""
GIT_REPOSITORY_DIRECTORY_NAME=""
WORKSPACE=~/workspace
AUTHORS_OK=false

POM_FILE=pom.xml
OLD_MAVEN_PARENT_VERSION="1.0.0"
NEW_MAVEN_PARENT_VERSION="1.1.0"
GITEA_TOKEN=

GIT_OK=false

function git_repository {
	
	
	while [ "$GIT_OK" = false ]
	do
		echo "Did you created the https://code-repo.d4science.org/gCubeSystem/${GIT_REPOSITORY_DIRECTORY_NAME}.git repository?"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	GIT_OK=true;
		        	break;;
		        No )
		        	GIT_OK=false
		        	git_create_repository
		        	break;;
		        Abort )
		        	exit;;
		    esac
		done
		
	done
}

function git_create_repository {
	local GIT_CREATE_OK=false
	
	if [[ -z "$GITEA_TOKEN" ]]; then
		echo "Please initialize GITEA_TOKEN first";
		exit
	fi
	
	while [ "$GIT_CREATE_OK" = false ]
	do
		echo "Do you want automatically create https://code-repo.d4science.org/gCubeSystem/${GIT_REPOSITORY_DIRECTORY_NAME}.git repository?"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	curl -X POST "https://code-repo.d4science.org/api/v1/org/gCubeSystem/repos?access_token=$GITEA_TOKEN" -H "accept: application/json" -H "content-type: application/json" -d "{\"name\":\"${GIT_REPOSITORY_DIRECTORY_NAME}\"}"
		        	echo "\n"
		        	GIT_CREATE_OK=true;
		        	GIT_OK=true;
		        	break;;
		        No )
		        	echo "Please created it at https://code-repo.d4science.org/repo/create?org=5722 and select yes to continue. See how to do it at https://wiki.gcube-system.org/gcube/Git_Repositories#Create_a_New_Repository"
		        	git_repository
		        	GIT_CREATE_OK=true
		        	break;;
		        Abort )
		        	exit;;
		    esac
		done
		
	done
}




function authors {
	if [ "$AUTHORS_OK" = false ]; then
		read -p "By continuing you will be able to check the authors file with 'less' command (type ENTER to continue)."
	fi
	
	while [ "$AUTHORS_OK" = false ]
	do
		less $AUTHORS_FILE
		echo "Are authors correct?"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	AUTHORS_OK=true;
		        	break;;
		        No )
		        	echo "Please edit $AUTHORS_FILE manually then continue"
		        	AUTHORS_OK=false
		        	break;;
		        Abort )
		        	AUTHORS_OK=false
		        	exit;;
		    esac
		done
		
		if [ $AUTHORS_OK = false ]; then
			nano $AUTHORS_FILE
		fi
		
	done
}

function repositories {
	local REPO_OK=false
	
	while [ "$REPO_OK" = false ]
	do
		git remote -v
		echo "Are repositories correct?"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	REPO_OK=true;
		        	break;;
		        No )
		        	REPO_OK=false
		        	exit;;
		        Abort )
		        	REPO_OK=false
		        	exit;;
		    esac
		done
		
	done
}

function push {
	local PUSH_OK=false
	
	while [ "$PUSH_OK" = false ]
	do
		echo "Do you want to push to the git repository?"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	PUSH_OK=true;
		        	git push --set-upstream --force origin master
		        	break;;
		        No )
		        	PUSH_OK=true
		        	break;;
		        Abort )
		        	PUSH_OK=false
		        	exit;;
		    esac
		done
		
	done
}

function fix_maven-parent {
	local FIX_OK=false
	
	while [ "$FIX_OK" = false ]
	do
		echo "Do you want to change maven parent from $OLD_MAVEN_PARENT_VERSION to $NEW_MAVEN_PARENT_VERSION?"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	FIX_OK=true
		        	sed -i "s/<version>$OLD_MAVEN_PARENT_VERSION<\/version>/<version>$NEW_MAVEN_PARENT_VERSION<\/version>/g" $POM_FILE
		        	break;;
		        No )
		        	FIX_OK=true
		        	break;;
		        Abort )
		        	exit;;
		    esac
		done
		
	done
}

function maven-parent-fixed {
	local FIXED=false
	
	while [ "$FIXED" = false ]
	do
		git diff
		echo "Was the fix made properly. Answering yes will result in a commit and a push to the push origin"
		select yna in "Yes" "No" "Abort";
		do
		    case $yna in
		        Yes )
		        	FIXED=true
		        	git add $POM_FILE
		        	git commit -m "Switched maven parent from $OLD_MAVEN_PARENT_VERSION to $NEW_MAVEN_PARENT_VERSION"
		        	break;;
		        No )
		        	exit;;
		    esac
		done
		
	done
}

function ignore_target_directory {
	echo target > .gitignore
	git add .gitignore
	git commit -m 'Ignored generated target directory'
}

function showhelp {
	echo -e "\nUsage:  import-from-svn.sh [-s <SVN-URL>] [-u <USERNAME>] [-h] \n"
	echo    " -s <SVN_URL> The SVN repository URL"
	echo    " -p <SVN_PROJECT_LOCAL_DIR> The path to the local project actually on SVN"
	echo	" -u <USERNAME> for git svn clone command e.g. luca.frosini"
	echo	" -g <GIT_REPOSITORY_DIRECTORY_NAME>"
	echo	" -a does not request to check the author file"
	echo -e " -h shows this help.\n"
}

while getopts ":i:s:p:u:g:a:h" opt; do
	case $opt in
		i) INIT=true;;
		s) SVN_URL=$OPTARG;;
		p) SVN_PROJECT_LOCAL_DIR=$OPTARG;;
		u) USERNAME=$OPTARG;;
		g) GIT_REPOSITORY_DIRECTORY_NAME=$OPTARG;;
		a) AUTHORS_OK=true;;
		h) showhelp;
    		exit ;;
		?) echo -e "\nERROR:invalid option: -$OPTARG"; 
			showhelp;
			exit 1 >&2;;
  	esac
done

GIT_MIGRATION_DIRECTORY="$(pwd)"

if [[ -n "$SVN_PROJECT_LOCAL_DIR" ]]; then
	echo "Going to migrate SVN repository : $SVN_PROJECT_LOCAL_DIR"
	cd $SVN_PROJECT_LOCAL_DIR
	
	if [[ -z "$SVN_URL" ]]; then
		SVN_URL="$(svn info --show-item url)"
		echo "SVN repository URL is : $SVN_URL"
	fi
	
	if [[ -z "$GIT_REPOSITORY_DIRECTORY_NAME" ]]; then
		GIT_REPOSITORY_DIRECTORY_NAME="$(basename $PWD)"
		echo "Local repository directory will be : $GIT_REPOSITORY_DIRECTORY_NAME"
	fi
fi

if [ $INIT ]; then
	wget https://code-repo.d4science.org/gCubeSystem/Configs/raw/branch/master/SVN/svn-migration-scripts.jar
	echo "You must configure the SVn to Git migration facilities. Please check how to do it at https://wiki.gcube-system.org/gcube/Import_from_SVN#Configure_your_system_to_allow_Migration_Facilities_to_properly_work"
	exit
fi

cd $GIT_MIGRATION_DIRECTORY

MIGRATION_DIRECTORY="${GIT_REPOSITORY_DIRECTORY_NAME}-migration"
echo "Going to create $MIGRATION_DIRECTORY"
rm -rf $MIGRATION_DIRECTORY
mkdir $MIGRATION_DIRECTORY
cd $MIGRATION_DIRECTORY

git_repository

echo -e "\n"
#echo "Removing old $AUTHORS_FILE file"
rm -rf $AUTHORS_FILE
#echo "Getting updated $AUTHORS_FILE file"
wget -q -O $AUTHORS_FILE https://code-repo.d4science.org/gCubeSystem/Configs/raw/branch/master/SVN/all-svn-authors.txt

#echo "Exporting authors from history of $SVN_URL repository"
#java -jar svn-migration-scripts.jar authors $SVN_URL > $AUTHORS_FILE
#sed -i s/@mycompany.com/@isti.cnr.it/g $AUTHORS_FILE
authors

git svn clone --authors-file=$AUTHORS_FILE --follow-parent $SVN_URL --username $USERNAME $GIT_REPOSITORY_DIRECTORY_NAME

cd $GIT_REPOSITORY_DIRECTORY_NAME
CREATED_GIT_DIRECTORY="$(pwd)"
echo "Git Repository directory $CREATED_GIT_DIRECTORY"
git branch
#git remote add origin "https://code-repo.d4science.org/gCubeSystem/${GIT_REPOSITORY_DIRECTORY_NAME}.git"
git remote add origin "gitea@code-repo.d4science.org:gCubeSystem/${GIT_REPOSITORY_DIRECTORY_NAME}.git"
git remote -v

push

SVN_DIRECTORY="${WORKSPACE}/SVN"
if [ ! -d "$SVN_DIRECTORY" ]; then
  mkdir $SVN_DIRECTORY
fi


mv $SVN_PROJECT_LOCAL_DIR ${SVN_DIRECTORY}/
mv $CREATED_GIT_DIRECTORY $SVN_PROJECT_LOCAL_DIR

cd $SVN_PROJECT_LOCAL_DIR
fix_maven-parent
maven-parent-fixed

ignore_target_directory
git push

cd $GIT_MIGRATION_DIRECTORY
rm -rf $MIGRATION_DIRECTORY

echo "Well Done"
echo "Please configure the Jenkins Building system for your project at https://jenkins.d4science.org/ See how to do it at https://wiki.gcube-system.org/gcube/Jenkins_Projects_(aka_Jobs)"
echo "Please Add Webhhoks on Gitea at https://code-repo.d4science.org/gCubeSystem/${GIT_REPOSITORY_DIRECTORY_NAME} See how to do it at https://wiki.gcube-system.org/gcube/Gitea/Jenkins:_Setting_up_Webhooks#Webhook_on_the_Gitea_repository"
