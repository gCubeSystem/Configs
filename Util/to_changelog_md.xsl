<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text" indent="no" encoding="UTF-8"/>

<xsl:template match="*/text()[normalize-space()]">
<xsl:text>&#10;</xsl:text>
<xsl:value-of select="normalize-space()"/>
</xsl:template>

<xsl:template match="/">
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
<xsl:for-each select="ReleaseNotes/Changeset">

## [v<xsl:value-of select="translate(translate(substring(@component, string-length(@component) - 5),'.',''),'-','.')" />] - <xsl:value-of select="@date" />

<xsl:for-each select="Change">
<xsl:text>&#10;</xsl:text>
<xsl:apply-templates/>
</xsl:for-each>
<xsl:text>&#10;</xsl:text>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>